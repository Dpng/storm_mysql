package com.mongoose.poc;
import com.mongoose.util.HibernateUtil;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;

public class ReadCustomerDetailsBolt extends BaseBasicBolt {
	
	private final Logger LOG = LoggerFactory.getLogger(ReadCustomerDetailsBolt.class);
	private static final long serialVersionUID = -9040752260298234425L;
	
	public void execute(Tuple input, BasicOutputCollector collector) {
		try {
		String id = input.getStringByField("Cust_ID");
		HibernateUtil.getInstance().getData(id);
		}
		catch(Exception ex) { 
			LOG.error("Failed in ReadCustomerDetailsBolt!!!! ", ex);
		}
		
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		
	}
}
