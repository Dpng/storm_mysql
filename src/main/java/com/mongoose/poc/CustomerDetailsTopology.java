package com.mongoose.poc;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.TopologyBuilder;

public class CustomerDetailsTopology {

	public static void main(String[] args) throws AlreadyAliveException,
			InvalidTopologyException {

		TopologyBuilder builder = new TopologyBuilder();

		builder.setSpout("CustomerIDSpout", new CustomerIDSpout(), 2);
		builder.setBolt("ReadCustomerDetailsBolt",
				new ReadCustomerDetailsBolt(), 4).shuffleGrouping(
				"CustomerIDSpout");

		Config conf = new Config();
		conf.setDebug(true);

		if (args != null && args.length > 0) {
			conf.setNumWorkers(1);

			try {
				StormSubmitter.submitTopology("CustomerDetailsTopology", conf,
						builder.createTopology());
			} catch (AlreadyAliveException alreadyAliveException) {
				System.out.println(alreadyAliveException);
			} catch (InvalidTopologyException invalidTopologyException) {
				System.out.println(invalidTopologyException);
			}
		} else {
			LocalCluster cluster = new LocalCluster();
			cluster.submitTopology("CustomerDetailsTopology", conf, builder.createTopology());

//			try {
//				Thread.sleep(10000);} 
//			catch (InterruptedException e) {
//				e.printStackTrace();
//			}

			//cluster.shutdown();
		}

	}

}
