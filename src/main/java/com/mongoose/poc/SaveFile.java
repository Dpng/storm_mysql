package com.mongoose.poc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import backtype.storm.tuple.Values;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

public class SaveFile {

	public static void main(String[] args) {
		RandomAccessFile fileReader;
		String file = "D:\\input.json";
		try {
			fileReader = new RandomAccessFile(file, "rw");

			Data d = new Data();
			d.setId("1");
			d.setFirstname("Lokesh");
			d.setLastname("Gupta");

			String s = new JSONSerializer().serialize(d);
			fileReader.writeChars(s);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			Scanner reader = new Scanner(new File(file), "UTF-16");
			String line;
			while (reader.hasNextLine()) {
				line = reader.nextLine();
				Data obj = new JSONDeserializer<Data>().deserialize(line);
				System.out.println(obj.getFirstname());
			}
			reader.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

}
