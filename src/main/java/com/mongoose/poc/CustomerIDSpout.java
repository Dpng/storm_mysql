package com.mongoose.poc;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

public class CustomerIDSpout extends BaseRichSpout {

	private static final long serialVersionUID = 1L;
	private SpoutOutputCollector spoutOutputCollector;

	private static final Map<Integer, String> map =    new HashMap<Integer, String>();  
	static {    map.put(0, "1");    
				map.put(1, "2");    
				map.put(2, "3");    
				map.put(3, "4");    
				map.put(4, "5");  
			}
	
	public void open(Map conf, TopologyContext context,	SpoutOutputCollector collector) {
		this.spoutOutputCollector = collector;
	}

	public void nextTuple() {
		int randomNumber = new Random().nextInt(5);
		System.out.println("RANDOM__NUMBER_DPN: "+map.get(randomNumber));
		this.spoutOutputCollector.emit(new Values(map.get(randomNumber)));
		
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("Cust_ID"));
	}

}
