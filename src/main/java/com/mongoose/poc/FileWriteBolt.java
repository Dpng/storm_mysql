package com.mongoose.poc;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;

public class FileWriteBolt extends BaseBasicBolt {

	public void execute(Tuple tuple, BasicOutputCollector arg1) {
		Data entry = (Data)tuple.getValueByField("input");
		System.out.println(entry.getFirstname());
	}

	public void declareOutputFields(OutputFieldsDeclarer arg0) {
		// TODO Auto-generated method stub

	}

}
