package com.mongoose.poc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Map;
import java.util.Scanner;

import flexjson.JSONDeserializer;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

public class FileReadSpout extends BaseRichSpout {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9040752260298234425L;
	SpoutOutputCollector _collector;
	Scanner reader;
	String file;

	public void nextTuple() {
		Utils.sleep(2000);
		String line = null;
		try {
			while (reader.hasNextLine()) {
				line = reader.nextLine();
				Data lineData = new JSONDeserializer<Data>().deserialize(line);
				_collector.emit(new Values(lineData));
			}
		} catch (Exception ex) {

		}
	}

	public void open(Map conf, TopologyContext context,
			SpoutOutputCollector collector) {
		_collector = collector;
		try {
			file = "D:\\input.json";
			reader = new Scanner(new File(file), "UTF-16");
		} catch (FileNotFoundException e) {
			System.exit(1);
		}
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("input"));
	}
}
