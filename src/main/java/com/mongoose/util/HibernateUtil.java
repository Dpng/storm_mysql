package com.mongoose.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongoose.data.CustomerDetails;



public class HibernateUtil {
	
	private final Logger LOG = LoggerFactory.getLogger(HibernateUtil.class);
	private Configuration config;
	private SessionFactory sessionFactory;
	
	public volatile static HibernateUtil singleton;
	
	private HibernateUtil(){
		try{
			config = new Configuration().configure("resources/hibernate.cfg.xml");
			ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(config.getProperties()).buildServiceRegistry();
            sessionFactory = config.buildSessionFactory(serviceRegistry);
            //session = sessionFactory.openSession();
		}catch( Throwable e){
			System.out.println("Initialization of Hibernate Configuration FAILED!!! "+ e);
			throw new ExceptionInInitializerError(e);
		}
		
	}
	
	public CustomerDetails getData(String id) {
		
		CustomerDetails cust = new CustomerDetails();
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		cust = (CustomerDetails)session.get(CustomerDetails.class, Integer.parseInt(id));
		System.out.println("Customer id: "+cust.getCustId()+" Customer FirstName: "+cust.getFirstName()+" Customer LastName: "+cust.getLastName());
		tx.commit();
		session.close();
		return cust;
	}
	
	public static HibernateUtil getInstance(){
		
		if(singleton == null){
			synchronized ( HibernateUtil.class){
				
				if(singleton == null){
					singleton = new HibernateUtil();
				}
			}
		}
		
		return singleton;
		
	}
	
	public void saveData(CustomerDetails cust) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(cust);
		session.getTransaction().commit();
		session.close();
	}


}
